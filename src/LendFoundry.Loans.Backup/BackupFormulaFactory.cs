﻿using LendFoundry.Configuration.Client;
using LendFoundry.Loans.Backup.Abstractions.Configuration;

namespace LendFoundry.Loans.Backup
{
    public class BackupFormulaFactory : IBackupFormulaFactory
    {
        public IBackupFormula Create(
            ILoanDetails loanDetails,
            IConfigurationService<BackupConfiguration> configurationService)
        {
            return new BackupFormula(loanDetails, configurationService);
        }
    }
}