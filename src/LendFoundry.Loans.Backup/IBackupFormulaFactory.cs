﻿using LendFoundry.Configuration.Client;
using LendFoundry.Loans.Backup.Abstractions.Configuration;

namespace LendFoundry.Loans.Backup
{
    public interface IBackupFormulaFactory
    {
        IBackupFormula Create(
            ILoanDetails loanDetails,
            IConfigurationService<BackupConfiguration> configurationService);
    }
}