﻿using System;

namespace LendFoundry.Loans.Backup
{
    public interface IBackupFormula
    {
        double TotalPrincipalPaid { get; }

        double TotalInterestPaid { get;  }

        double TotalFeesPaid { get; }

        double TotalPaymentsMade { get; }

        double TotalPaymentsRemaining { get; }

        // TODO: Item 4 `Card#90`

        double TotalPrincipalBalance { get; }

        double TotalInterestBalance { get; }

        //double TotalFeesBalance { get; }

        // TODO: Item 6 `Card#90`

        DateTimeOffset? MaturityDate { get; }
    }
}