﻿using LendFoundry.ActivityLog.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Loans.Backup
{
    public class BackupListener : IBackupListener
    {
        public BackupListener
        (
            IBackupServiceFactory serviceFactory,
            IConfigurationServiceFactory<BackupConfiguration> configurationFactory,
            IEventHubClientFactory eventHubFactory,
            ITokenHandler tokenHandler,
            ITenantServiceFactory tenantServiceFactory,
            ILoanServiceFactory loanServiceFactory,
            ILoggerFactory loggerFactory,
            IActivityLogClientFactory activityLogFactory
        )
        {
            ServiceFactory = serviceFactory;
            ConfigurationFactory = configurationFactory;
            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            TenantServiceFactory = tenantServiceFactory;
            LoanServiceFactory = loanServiceFactory;
            LoggerFactory = loggerFactory;
            ActivityLogFactory = activityLogFactory;
        }

        private IBackupServiceFactory ServiceFactory { get; }

        private IConfigurationServiceFactory<BackupConfiguration> ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ILoanServiceFactory LoanServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }        

        private IActivityLogClientFactory ActivityLogFactory { get; }


        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Loan Backup listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var eventhub = EventHubFactory.Create(emptyReader);

                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var loanService = LoanServiceFactory.Create(reader);
                    var activityLogService = ActivityLogFactory.Create(reader);

                    var configurationService = ConfigurationFactory.Create(reader);
                    var configuration = configurationService.Get();
                    if (configuration != null)
                    {
                        var backupService = ServiceFactory.Create(reader, logger, ConfigurationFactory);
                        foreach (var eventConfiguration in configuration.Events)
                        {
                            eventhub.On(eventConfiguration.Name, backupService.ProcessEvent(eventConfiguration, loanService, logger, activityLogService));
                            logger.Info($"Attached eventhub subscription for  #{eventConfiguration.Name}");
                        }
                    }
                    else
                    {
                        logger.Info("The configuration can't be found");
                    }
                });
                eventhub.StartAsync();
                logger.Info("Loan Backup listener started");
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                logger.Error("Error while listening eventhub to process loan backup", ex);
                logger.Info("Trying to reset service..");
                Start();
            }
        }
    }
}