﻿using LendFoundry.ActivityLog;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Client;
using System.Collections.Generic;
using System;
using System.Linq;
using Newtonsoft.Json;
using System.Dynamic;

namespace LendFoundry.Loans.Backup
{
    public class BackupService : IBackupServiceExtended
    {
        public BackupService
            (
                IBackupRepository repository,
                ILogger logger,
                IConfigurationService<BackupConfiguration> configurationService,
                IBackupFormulaFactory backupFormulaFactory
            )
        {
            Repository = repository;
            Logger = logger;
            ConfigurationService = configurationService;
            BackupFormulaFactory = backupFormulaFactory;
        }

        private IBackupRepository Repository { get; }

        private ILogger Logger { get; }

        private IConfigurationService<BackupConfiguration> ConfigurationService { get; }

        private IBackupFormulaFactory BackupFormulaFactory { get; }

        public IEnumerable<IBackupEntry> GetBackups(DateTimeOffset? start, int? page = null)
        {
            var configuration = ConfigurationService.Get();

            if (configuration == null)
                throw new ArgumentException("The configuration can't be found");

            var pageSize = configuration.PageSize;

            if (pageSize == int.MinValue || pageSize == 0)
                throw new ArgumentException($"#{nameof(pageSize)} must to be informed");

            var skip = page.HasValue
                ? pageSize * (page - 1 < 0 ? (page - 1) * -1 : page - 1)
                : null;

            return Repository.Since(start, skip, pageSize);
        }

        public IBackupEntry GetBackup(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException("Loan reference number must to be informed.");

            return Repository.GetBackup(loanReferenceNumber);
        }

        public Action<EventInfo> ProcessEvent(EventMapping eventMapping, ILoanService loanService, ILogger logger, IActivityLogService activityLogService)
        {
            return @event =>
            {
                try
                {
                    var loanReferenceNumber = eventMapping.LoanReferenceNumber.FormatWith(@event);

                    if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                    {
                        throw new ArgumentException($"{nameof(loanReferenceNumber)} can't be null");
                    }

                    logger.Info($"Received #{@event.Name} from #{@event.TenantId}");

                    var loanDetails = loanService.GetLoanDetails(loanReferenceNumber);
                    if (loanDetails != null)
                    {
                        var activityLogEntries = activityLogService.GetActivities("loan", loanReferenceNumber);

                        BuildBackup(loanDetails, @event.Time, activityLogEntries);
                        logger.Info($"Loan #{loanReferenceNumber} recorded successfully");
                    }
                    else
                    {
                        logger.Warn($"Loan #{loanReferenceNumber} could not be found");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Error while processing event #{@event.Name}", ex);
                }
            };
        }

        private void BuildBackup(ILoanDetails loanDetails, TimeBucket snapshotTime, IEnumerable<IActivityLog> activityLogEntries)
        {
            if (loanDetails == null)
                throw new ArgumentException($"{nameof(loanDetails)} cannot be null");

            if (string.IsNullOrWhiteSpace(loanDetails.LoanInfo.Loan.ReferenceNumber))
                throw new ArgumentException("A backup must to have a LoanReferenceNumber");

            var listOfActivities = new List<IActivityLog>(activityLogEntries.Select(a => 
            {
                // Convert `Data` to ExpandoObject
                if (a.Event.Data != null)
                {
                    try
                    {
                        a.Event.Data = JsonConvert.DeserializeObject<ExpandoObject>(a.Event.Data.ToString());
                    }
                    catch (JsonException ex)
                    {
                        Logger.Error($"Error while trying deserialize object in event #{a.Event.Name}. Error: {ex.Message}");
                        a.Event.Data = null;
                    }
                }

                return a;
            }).ToArray());

            var entry = new BackupEntry(loanDetails, snapshotTime, listOfActivities);

            // Calculations
            var backupFormula = BackupFormulaFactory.Create(loanDetails, ConfigurationService);

            // Filling up of the new fields 
            entry.TotalPrincipalPaid = backupFormula.TotalPrincipalPaid;
            entry.TotalInterestPaid = backupFormula.TotalInterestPaid;

            // TODO: Verify the FeeCodes with @eduardod
            entry.TotalFeesPaid = backupFormula.TotalFeesPaid;

            entry.TotalPaymentsMade = backupFormula.TotalPaymentsMade;
            entry.TotalPaymentsRemaining = backupFormula.TotalPaymentsRemaining;

            // TODO: Card#90 Item - 4) Payment Type (not coming back)

            entry.TotalPrincipalBalance = backupFormula.TotalPrincipalBalance;
            entry.TotalInterestBalance = backupFormula.TotalInterestBalance;

            // TODO: Verify this field with @eduardod
            //entry.TotalFeesBalance = backupFormula.TotalFeesBalance;

            // TODO: Card#90 Item - 6) Interest paid through, principle paid through (date fields)

            entry.MaturityDate = backupFormula.MaturityDate;

            Repository.AddOrUpdate(entry);
            Logger.Info($"Backup for loan #{loanDetails.LoanInfo.Loan.ReferenceNumber} added in database");
        }
    }
}