﻿using LendFoundry.Configuration.Client;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LendFoundry.Loans.Backup
{
    public class BackupFormula : IBackupFormula
    {
        private ILoanDetails LoanDetails { get; }

        private IConfigurationService<BackupConfiguration> ConfigurationService { get; } 

        public BackupFormula(
            ILoanDetails loanDetails, 
            IConfigurationService<BackupConfiguration> configurationService)
        {
            LoanDetails = loanDetails;
            ConfigurationService = configurationService;
        }

        /// <summary>
        /// Principal Paid: SUM(item.paymentSchedule.installments.$.principal) where status = "completed"
        /// </summary>
        public double TotalPrincipalPaid
        {
            get
            {
                if (LoanDetails.PaymentSchedule.Installments == null)
                    return 0;

                return LoanDetails.PaymentSchedule.Installments
                    .Where(i => i.Status == InstallmentStatus.Completed)
                    .Sum(i => i.Principal);
            }
        }

        /// <summary>
        /// Interest Paid: SUM(item.paymentSchedule.installments.$.interest) where status = "completed"
        /// </summary>
        public double TotalInterestPaid
        {
            get
            {
                if (LoanDetails.PaymentSchedule.Installments == null)
                    return 0;

                return LoanDetails.PaymentSchedule.Installments
                    .Where(i => i.Status == InstallmentStatus.Completed)
                    .Sum(i => i.Interest);
            }
        }

        /// <summary>
        /// Fees Paid: SUM(item.transactions.$.amount) where code in (list of fee codes, we will provide all the codes we have)
        /// </summary>
        public double TotalFeesPaid
        {
            get
            {
                if (LoanDetails.Transactions == null)
                    return 0;

                return LoanDetails.Transactions
                    .Where(t => GetFeeCodes().Any(code => code == t.Code))
                    .Sum(t => t.Amount);
            }
        }

        /// <summary>
        /// Payments made: SUM(item.paymentSchedule.installments.$.paymentAmount) where status = "completed"
        /// </summary>
        public double TotalPaymentsMade
        {
            get
            {
                if (LoanDetails.PaymentSchedule.Installments == null)
                    return 0;

                return LoanDetails.PaymentSchedule.Installments
                    .Where(i => i.Status == InstallmentStatus.Completed)
                    .Sum(i => i.PaymentAmount);
            }
        }

        /// <summary>
        /// Payments remaining: SUM(item.paymentSchedule.installments.$.paymentAmount) where status = "scheduled"
        /// </summary>
        public double TotalPaymentsRemaining
        {
            get
            {
                if (LoanDetails.PaymentSchedule.Installments == null)
                    return 0;

                return LoanDetails.PaymentSchedule.Installments
                    .Where(i => i.Status == InstallmentStatus.Scheduled)
                    .Sum(i => i.PaymentAmount);
            }
        }

        /// <summary>
        /// Principal Balance: SUM(item.paymentSchedule.installments.$.principal) where status = "scheduled"
        /// </summary>
        public double TotalPrincipalBalance
        {
            get
            {
                if (LoanDetails.PaymentSchedule.Installments == null)
                    return 0;

                return LoanDetails.PaymentSchedule.Installments
                    .Where(i => i.Status == InstallmentStatus.Scheduled)
                    .Sum(i => i.Principal);
            }
        }

        /// <summary>
        /// Interest Balance: SUM(item.paymentSchedule.installments.$.interest) where status = "scheduled"
        /// </summary>
        public double TotalInterestBalance
        {
            get
            {
                if (LoanDetails.PaymentSchedule.Installments == null)
                    return 0;

                return LoanDetails.PaymentSchedule.Installments
                    .Where(i => i.Status == InstallmentStatus.Scheduled)
                    .Sum(i => i.Interest);
            }
        }

        /// <summary>
        /// Fee Balance: TODO: we will work to check the best way of exposing this information.
        /// </summary>
        //public double TotalFeesBalance
        //{
        //    get
        //    {
        //        //if (LoanDetails.PaymentSchedule.Installments == null)
        //            return 0;

        //        //return LoanDetails.PaymentSchedule.Installments
        //        //    .Where(i => i.Status == InstallmentStatus.Scheduled)
        //        //    .Sum(i => i.);
        //    }
        //}

        /// <summary>
        /// Maturity Date: LAST(item.paymentSchedule.installment.$.anniversaryDate)
        /// </summary>
        public DateTimeOffset? MaturityDate
        {
            get
            {
                var last = LoanDetails.PaymentSchedule.Installments?
                    .OrderBy(i => i.AnniversaryDate)
                    .LastOrDefault();

                return last?.AnniversaryDate;
            }
        }


        private IEnumerable<string> GetFeeCodes()
        {
            try
            {
                var configuration = ConfigurationService.Get();
                if (configuration == null)
                    throw new ArgumentNullException("The configuration cannot be found");

                if (configuration.LoanFees == null)
                    throw new ArgumentNullException($"The configuration related to '{nameof(configuration.LoanFees)}' cannot be null");

                return configuration.LoanFees.Select(t => t.Code);
            }
            catch (Exception ex)
            {
                throw new Exception("Unhandled exception while 'GetFeeCodes' was called. Erro:", ex);
            }
        }
    }
}