﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Backup
{
    public class BackupServiceFactory : IBackupServiceFactory
    {
        public BackupServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IBackupServiceExtended Create(
            StaticTokenReader reader, 
            ILogger logger, 
            IConfigurationServiceFactory<BackupConfiguration> configurationServiceFactory)
        {
            var repositoryFactory = Provider.GetService<IBackupRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var configurationService = configurationServiceFactory.Create(reader);
            var backupFormulaFactory = Provider.GetService<IBackupFormulaFactory>();

            return new BackupService(repository, logger, configurationService, backupFormulaFactory);
        }
    }
}