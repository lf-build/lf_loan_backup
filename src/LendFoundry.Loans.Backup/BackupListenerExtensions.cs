﻿using LendFoundry.Loans.Backup.Abstractions;
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Backup
{
    public static class BackupListenerExtensions
    {
        public static void UseBackupListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IBackupListener>().Start();
        }
    }
}