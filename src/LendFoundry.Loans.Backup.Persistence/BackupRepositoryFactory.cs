﻿using LendFoundry.Loans.Backup.Abstractions;
using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Backup.Persistence
{
    public class BackupRepositoryFactory : IBackupRepositoryFactory
    {
        private IServiceProvider Provider { get; }

        public BackupRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IBackupRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new BackupRepository(tenantService, mongoConfiguration);
        }
    }
}