﻿using LendFoundry.ActivityLog;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Schedule;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.Loans.Backup.Persistence
{
    public class BackupRepository : MongoRepository<IBackupEntry, BackupEntry>, IBackupRepository
    {
        static BackupRepository()
        {
            BsonClassMap.RegisterClassMap<BackupEntry>(map =>
            {
                map.AutoMap();

                map.MapMember(p => p.MaturityDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(BackupEntry);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);

                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoan, Loan>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IBankAccount, BankAccount>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IBorrower, Borrower>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoanInvestor, LoanInvestor>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IScore, Score>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoanTerms, LoanTerms>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IFee, Fee>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IPaymentScheduleSummary, PaymentScheduleSummary>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IPaymentSchedule, PaymentSchedule>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IInstallment, Installment>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ITransaction, Transaction>());
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IActivityLog, ActivityLog.ActivityLog>());
            });

            BsonClassMap.RegisterClassMap<ActivityLog.ActivityLog>(map =>
            {
                map.AutoMap();
                var type = typeof(ActivityLog.ActivityLog);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public BackupRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "loan-backup")
        {
            CreateIndexIfNotExists
            (
                indexName: "loan-reference-number",
                index: Builders<IBackupEntry>.IndexKeys
                    .Ascending(i => i.TenantId)
                    .Ascending(i => i.Loan.ReferenceNumber)
            );

            CreateIndexIfNotExists
            (
                indexName: "snapshot-time",
                index: Builders<IBackupEntry>.IndexKeys
                    .Ascending(i => i.TenantId)
                    .Ascending(i => i.SnapshotTime)
            );
        }

        public void AddOrUpdate(IBackupEntry backup)
        {
            if (backup == null)
                throw new ArgumentNullException(nameof(backup));

            if (string.IsNullOrWhiteSpace(backup.Loan.ReferenceNumber))
                throw new ArgumentException("Loan Reference Number must to be informed.");

            var existingId = Query
                .Where(l => l.Loan.ReferenceNumber == backup.Loan.ReferenceNumber)
                .Select(v => v.Id)
                .FirstOrDefault();

            backup.TenantId = TenantService.Current.Id;
            backup.Id = existingId ?? ObjectId.GenerateNewId().ToString();

            if (string.IsNullOrWhiteSpace(existingId))
            {
                Collection.InsertOne(backup);
            }
            else
            {
                Expression<Func<IBackupEntry, bool>> backupAlreadyCreated = l =>
                    l.TenantId == TenantService.Current.Id &&
                    l.Loan.ReferenceNumber == backup.Loan.ReferenceNumber;

                Collection.ReplaceOne(backupAlreadyCreated, backup);
            }
        }

        public IEnumerable<IBackupEntry> Since(DateTimeOffset? start, int? skip = null, int? quantity = null)
        {
            var cursor = Collection
                .Aggregate(new AggregateOptions { AllowDiskUse = true })
                .Match(q => q.TenantId == TenantService.Current.Id);

            if (start.HasValue)
                cursor = cursor.Match(q => q.SnapshotTime.Time > start);

            cursor = cursor.SortBy(b => b.SnapshotTime.Time);

            if (skip.HasValue)
                cursor = cursor.Skip(skip.Value);

            if (quantity.HasValue)
                cursor = cursor.Limit(quantity.Value);

            return cursor.ToList();
        }

        public IBackupEntry GetBackup(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException("Loan reference number must to be informed.");

            return Query.FirstOrDefault(b => b.Loan.ReferenceNumber == loanReferenceNumber);
        }
    }
}