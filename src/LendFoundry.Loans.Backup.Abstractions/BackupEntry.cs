﻿using LendFoundry.ActivityLog;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans.Schedule;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Backup.Abstractions
{
    public class BackupEntry : Aggregate, IBackupEntry
    {
        public BackupEntry() {}

        public BackupEntry
            (
                ILoanDetails loanDetails, 
                TimeBucket snapshotTime,
                IEnumerable<IActivityLog> activityLog
            )
        {
            Loan = loanDetails.LoanInfo.Loan;
            Summary = loanDetails.LoanInfo.Summary;
            PaymentSchedule = loanDetails.PaymentSchedule;
            Transactions = loanDetails.Transactions;
            SnapshotTime = snapshotTime;
            ActivityLog = activityLog;
        }

        public TimeBucket SnapshotTime { get; set; }

        public ILoan Loan { get; set; }

        public IPaymentScheduleSummary Summary { get; set; }

        public IPaymentSchedule PaymentSchedule { get; set; }

        public IEnumerable<ITransaction> Transactions { get; set; }


        public double TotalPrincipalPaid { get; set; }

        public double TotalInterestPaid { get; set; }

        public double TotalFeesPaid { get; set; }

        public double TotalPaymentsMade { get; set; }

        public double TotalPaymentsRemaining { get; set; }

        public double TotalPrincipalBalance { get; set; }

        public double TotalInterestBalance { get; set; }

        //public double TotalFeesBalance { get; set; }

        public DateTimeOffset? MaturityDate { get; set; }

        public IEnumerable<IActivityLog> ActivityLog { get; set; }
    }
}