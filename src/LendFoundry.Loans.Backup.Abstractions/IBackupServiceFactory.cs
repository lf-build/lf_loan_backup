﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Backup.Abstractions
{
    public interface IBackupServiceFactory
    {
        IBackupServiceExtended Create
            (
                StaticTokenReader reader, 
                ILogger logger, 
                IConfigurationServiceFactory<BackupConfiguration> configurationServiceFactory
            );
    }
}