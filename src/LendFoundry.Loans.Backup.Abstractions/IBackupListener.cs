﻿namespace LendFoundry.Loans.Backup.Abstractions
{
    public interface IBackupListener
    {
        void Start();
    }
}