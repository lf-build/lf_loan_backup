﻿using System;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Loans.Backup.Abstractions
{
    public interface IBackupRepository : IRepository<IBackupEntry>
    {
        void AddOrUpdate(IBackupEntry backup);

        IEnumerable<IBackupEntry> Since(DateTimeOffset? start, int? skip = null, int? quantity = null);

        IBackupEntry GetBackup(string loanReferenceNumber);
    }
}