﻿using LendFoundry.ActivityLog;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans.Schedule;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Backup.Abstractions
{
    public interface IBackupEntry : IAggregate
    {
        ILoan Loan { get; set; }

        IPaymentScheduleSummary Summary { get; set; }

        IPaymentSchedule PaymentSchedule { get; set; }

        IEnumerable<ITransaction> Transactions { get; set; }

        TimeBucket SnapshotTime { get; set; }

        double TotalPrincipalPaid { get; set; }

        double TotalInterestPaid { get; set; }

        double TotalFeesPaid { get; set; }

        double TotalPaymentsMade { get; set; }

        double TotalPaymentsRemaining { get; set; }

        // TODO: Item 4 `Card#90`
        
        double TotalPrincipalBalance { get; set; }

        double TotalInterestBalance { get; set; }

        //double TotalFeesBalance { get; set; }

        // TODO: Item 6 `Card#90`

        DateTimeOffset? MaturityDate { get; set; }

        IEnumerable<IActivityLog> ActivityLog { get; set; } 
    }
}