﻿namespace LendFoundry.Loans.Backup.Abstractions.Configuration
{
    public class BackupConfiguration
    {
        public EventMapping[] Events { get; set; }

        public int PageSize { get; set; }

        public LoanFees[] LoanFees { get; set; }
    }
}