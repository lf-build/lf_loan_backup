﻿namespace LendFoundry.Loans.Backup.Abstractions.Configuration
{
    public class LoanFees
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}