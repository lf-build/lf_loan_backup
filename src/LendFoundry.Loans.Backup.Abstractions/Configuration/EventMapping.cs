﻿namespace LendFoundry.Loans.Backup.Abstractions.Configuration
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string LoanReferenceNumber { get; set; }
    }
}