﻿using LendFoundry.ActivityLog;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Client;
using System.Collections.Generic;
using System;

namespace LendFoundry.Loans.Backup.Abstractions
{
    public interface IBackupService
    {
        IEnumerable<IBackupEntry> GetBackups(DateTimeOffset? start, int? page = default(int?));

        IBackupEntry GetBackup(string loanReferenceNumber);
    }

    public interface IBackupServiceExtended : IBackupService
    {
        Action<EventInfo> ProcessEvent(EventMapping eventMapping, ILoanService loanService, ILogger logger, IActivityLogService activityLogService);
    }
}