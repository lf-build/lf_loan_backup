﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Backup.Abstractions
{
    public interface IBackupRepositoryFactory
    {
        IBackupRepository Create(ITokenReader reader);
    }
}