﻿using LendFoundry.ActivityLog.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Backup.Persistence;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Backup.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<BackupConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLoanService(Settings.Loan.Host, Settings.Loan.Port);
            services.AddActivityHistory(Settings.ActivityLog.Host, Settings.ActivityLog.Port);

            // interface implements
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IBackupRepository, BackupRepository>();
            services.AddTransient<IBackupService, BackupService>();
            services.AddTransient<IBackupRepositoryFactory, BackupRepositoryFactory>();
            services.AddTransient<IBackupServiceFactory, BackupServiceFactory>();
            services.AddTransient<IBackupListener, BackupListener>();
            services.AddTransient<IBackupFormulaFactory, BackupFormulaFactory>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseBackupListener();
            app.UseMvc();
        }
    }
}