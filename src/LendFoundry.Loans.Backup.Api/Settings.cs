﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Loans.Backup.Api
{
    public class Settings
    {
        private const string Prefix = "LOAN_BACKUP";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Loan { get; } = new ServiceSettings($"{Prefix}_LOAN", "loans");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "loan-backup");

        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "loan-backup";

        public static ServiceSettings ActivityLog { get; } = new ServiceSettings($"{Prefix}_ACTIVITYLOG", "activitylog");
    }
}