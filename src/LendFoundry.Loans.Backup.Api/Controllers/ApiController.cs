﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Backup.Abstractions;
using Microsoft.AspNet.Mvc;
using System;
using System.Linq;

namespace LendFoundry.Loans.Backup.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IBackupService service, ILogger logger) : base(logger)
        {
            Service = service;
        }

        private IBackupService Service { get; }

        [HttpGet("/{start?}/{page?}")]
        public IActionResult GetBackups([FromRoute]DateTimeOffset? start, [FromRoute]int? page = null)
        {
            return Execute(() =>
            {
                var backups = Service.GetBackups(start, page);
                if (backups.Any())
                {
                    return new HttpOkObjectResult(backups);
                }
                throw new NotFoundException("not found backups");
                
            });
        }

        [HttpGet("/loan/{loanReferenceNumber}")]
        public IActionResult GetBackup([FromRoute]string loanReferenceNumber)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                    throw new ArgumentException("The loan reference number must to be informed.");

                var backup = Service.GetBackup(loanReferenceNumber);
                if (backup == null)
                    throw new NotFoundException($"The backup was not found with loan reference number: #{loanReferenceNumber}");

                return new HttpOkObjectResult(backup);
            });
        }
    }
}