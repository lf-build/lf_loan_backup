FROM registry.lendfoundry.com/base:beta8

ADD ./global.json /app/

ADD ./src/LendFoundry.Loans.Backup.Abstractions /app/LendFoundry.Loans.Backup.Abstractions
WORKDIR /app/LendFoundry.Loans.Backup.Abstractions
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Backup.Persistence /app/LendFoundry.Loans.Backup.Persistence
WORKDIR /app/LendFoundry.Loans.Backup.Persistence
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Backup /app/LendFoundry.Loans.Backup
WORKDIR /app/LendFoundry.Loans.Backup
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Backup.Api /app/LendFoundry.Loans.Backup.Api
WORKDIR /app/LendFoundry.Loans.Backup.Api
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel