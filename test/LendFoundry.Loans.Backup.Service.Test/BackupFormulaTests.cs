﻿using LendFoundry.Foundation.Testing.Date;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Backup.Repository.Test;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Backup.Service.Test
{
    public class BackupFormulaTests : InMemoryObjects
    {
        private TenantTimeMock TenantTime { get; set; } = new TenantTimeMock();

        //private Mock<IConfigurationService<BackupConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<BackupConfiguration>>();

        private ILoanDetails LoanDetails { get; } = GetLoanDetails("loan001");

        private new IBackupFormula BackupFormula => new BackupFormula(
            MockLoanDetail.Object,
            ConfigurationService.Object);

        private DateTimeOffset Date(int year, int month, int day)
        {
            return TenantTime.Create(year, month, day);
        }

        private Mock<ILoanDetails> MockLoanDetail { get; } = new Mock<ILoanDetails>();

        private BackupConfiguration BackupConfiguration => new BackupConfiguration
        {
            LoanFees = new LoanFees[]
            {
                new LoanFees { Code = "142", Name = "OriginationFee" },
                new LoanFees { Code = "150", Name = "Late Fee" },
                new LoanFees { Code = "180", Name = "ACH Return Fee" },
                new LoanFees { Code = "182", Name = "Check Return Fee" },
                new LoanFees { Code = "103", Name = "Check Processing Fee" },
                new LoanFees { Code = "452", Name = "Write Off Fee" },
                new LoanFees { Code = "626", Name = "Forbearance Fee" },
                new LoanFees { Code = "724", Name = "Modification Fee" },
                new LoanFees { Code = "140", Name = "Fee (Other)" }
            }
        };


        [Fact]
        public void TotalPrincipalPaid()
        {
            // arrange
            var installments = new[]
            {
                new Installment { Principal = 100, Status = InstallmentStatus.Completed },
                new Installment { Principal = 250, Status = InstallmentStatus.Completed },
                new Installment { Principal = 430, Status = InstallmentStatus.Completed },
                new Installment { Principal = 60, Status = InstallmentStatus.Scheduled }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(780, BackupFormula.TotalPrincipalPaid);
        }

        [Fact]
        public void TotalInterestPaid()
        {
            // arrange
            var installments = new[]
            {
                new Installment { Interest = 6.87, Status = InstallmentStatus.Completed },
                new Installment { Interest = 4.59, Status = InstallmentStatus.Completed },
                new Installment { Interest = 2.3, Status = InstallmentStatus.Scheduled }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(11.46, BackupFormula.TotalInterestPaid);
        }

        [Fact]
        public void TotalFeesPaid()
        {
            // arrange
            var transactions = new[]
            {
                new Transaction { Code = "140", Amount = 60.00},
                new Transaction { Code = "141", Amount = 60.00},
                new Transaction { Code = "142", Amount = 60.00}
            };
            MockLoanDetail.Setup(s => s.Transactions).Returns(transactions);

            ConfigurationService.Setup(x => x.Get()).Returns(BackupConfiguration);

            // act
            // assert
            Assert.Equal(120.00, BackupFormula.TotalFeesPaid);
        }

        [Fact]
        public void TotalPaymentsMade()
        {
            // arrange
            var installments = new[]
            {
                new Installment { PaymentAmount = 100, Status = InstallmentStatus.Completed },
                new Installment { PaymentAmount = 250, Status = InstallmentStatus.Completed },
                new Installment { PaymentAmount = 60, Status = InstallmentStatus.Scheduled },
                new Installment { PaymentAmount = 450, Status = InstallmentStatus.Completed }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(800, BackupFormula.TotalPaymentsMade);
        }

        [Fact]
        public void TotalPaymentsRemaining()
        {
            // arrange
            var installments = new[]
            {
                new Installment { PaymentAmount = 100, Status = InstallmentStatus.Scheduled },
                new Installment { PaymentAmount = 250, Status = InstallmentStatus.Scheduled },
                new Installment { PaymentAmount = 60, Status = InstallmentStatus.Completed },
                new Installment { PaymentAmount = 450, Status = InstallmentStatus.Scheduled }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(800, BackupFormula.TotalPaymentsRemaining);
        }

        [Fact]
        public void TotalPrincipalBalance()
        {
            // arrange
            var installments = new[]
            {
                new Installment { Principal = 100, Status = InstallmentStatus.Scheduled },
                new Installment { Principal = 250, Status = InstallmentStatus.Scheduled },
                new Installment { Principal = 60, Status = InstallmentStatus.Completed },
                new Installment { Principal = 450, Status = InstallmentStatus.Scheduled }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(800, BackupFormula.TotalPrincipalBalance);
        }

        [Fact]
        public void TotalInterestBalance()
        {
            // arrange
            var installments = new[]
            {
                new Installment { Interest = 6.87, Status = InstallmentStatus.Scheduled },
                new Installment { Interest = 4.59, Status = InstallmentStatus.Scheduled },
                new Installment { Interest = 2.3, Status = InstallmentStatus.Completed }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(11.46, BackupFormula.TotalInterestBalance);
        }

        [Fact]
        public void MaturityDate()
        {
            // arrange
            var installments = new[]
            {
                new Installment { AnniversaryDate = Date(2016, 04, 15) },
                new Installment { AnniversaryDate = Date(2016, 05, 15) },
                new Installment { AnniversaryDate = Date(2016, 06, 15) },
                new Installment { AnniversaryDate = Date(2016, 07, 15) }
            };

            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments).Returns(installments);

            // act
            // assert
            Assert.Equal(Date(2016, 07, 15), BackupFormula.MaturityDate);
        }

        [Fact]
        public void MaturityDate_When_HasNoValue()
        {
            // arrange
            MockLoanDetail.Setup(x => x.PaymentSchedule.Installments);

            // act
            // assert
            Assert.Null(BackupFormula.MaturityDate);
        }
    }
}