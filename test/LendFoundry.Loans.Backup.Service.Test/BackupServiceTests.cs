﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Backup.Repository.Test;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Backup.Service.Test
{
    public class BackupServiceTests : InMemoryObjects
    {
        // auxiliar
        private IBackupListener Listener
        {
            get
            {
                return new BackupListener
                (
                    BackupServiceFactory.Object,
                    ConfigurationServiceFactory.Object,
                    EventHubClientFactory.Object,
                    TokenHandler.Object,
                    TenantServiceFactory.Object,
                    LoanServiceFactory.Object,
                    LoggerFactory.Object,
                    ActivityLogFactory.Object
                );
            }
        }

        private IBackupServiceExtended Service
        {
            get
            {
                return new BackupService
                (
                    BackupRepository.Object,
                    Logger.Object,
                    ConfigurationService.Object,
                    BackupFormulaFactory.Object
                );
            }
        }

        private IEnumerable<TenantInfo> InMemoryTenants
        {
            get
            {
                return new[]
                {
                    new TenantInfo { Id = "my-tenant" },
                };
            }
        }

        private BackupConfiguration InMemoryConfiguration
        {
            get
            {
                return new BackupConfiguration
                {
                    Events = new[]
                    {
                        new EventMapping { Name = "my-event-1", LoanReferenceNumber = "{Data.LoanReferenceNumber}" },
                        new EventMapping { Name = "my-event-2", LoanReferenceNumber = "{Data.LoanReferenceNumber}" },
                    },
                    PageSize = 100
                };
            }
        }


        [Fact]
        public void Start_WhenExecutionOk()
        {
            var logContext = NullLogContext.Instance;
            var logInfo = It.IsAny<string>();
            var eventName = It.IsAny<string>();

            // arrange
            //-------------------------------
            Logger.Setup(s => s.Info(logInfo));
            Logger.Setup(s => s.Error(logInfo));
            LoggerFactory.Setup(s => s.Create(logContext)).Returns(Logger.Object);
            //-------------------------------
            EventHubClient.Setup(s => s.StartAsync());
            EventHubClient.Setup(s => s.On(eventName, It.IsAny<Action<EventInfo>>()));
            EventHubClientFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(EventHubClient.Object);
            //-------------------------------
            TenantService.Setup(s => s.GetActiveTenants()).Returns(InMemoryTenants.ToList());
            TenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(TenantService.Object);
            //-------------------------------
            TokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
            //-------------------------------
            ConfigurationService.Setup(s => s.Get()).Returns(InMemoryConfiguration);
            ConfigurationServiceFactory.Setup(c => c.Create(It.IsAny<StaticTokenReader>())).Returns(ConfigurationService.Object);
            //-------------------------------
            LoanService.Setup(l => l.GetLoanDetails(It.IsAny<string>())).Returns(Mock.Of<ILoanDetails>());
            LoanServiceFactory.Setup(l => l.Create(It.IsAny<StaticTokenReader>())).Returns(LoanService.Object);
            //-------------------------------
            var activityLogEntries = new[] { InMemoryActivity, InMemoryActivity, InMemoryActivity }
            .Select((a, i) => 
            {
                i++;
                a.EntityType = "loan";
                a.EntityId = $"loan00{i}";
                return a;
            });
            ActivityLogService.Setup(x => x.GetActivities(It.IsAny<string>(), It.IsAny<string>(), 3, 3)).Returns(activityLogEntries);
            ActivityLogFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(ActivityLogService.Object);

            //-------------------------------
            BackupService.Setup(s => s.ProcessEvent(It.IsAny<EventMapping>(), LoanService.Object, Logger.Object, ActivityLogService.Object));
            BackupServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>(), Logger.Object, It.IsAny<IConfigurationServiceFactory<BackupConfiguration>>())).Returns(BackupService.Object);
            //-------------------------------

            // act
            Listener.Start();

            // assert
            var tenantIterations = InMemoryConfiguration.Events.Length * InMemoryTenants.Count();
            Logger.Verify(v => v.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(v => v.Error(It.IsAny<string>()), Times.Never);

            EventHubClient.Verify(v => v.On(It.IsAny<string>(), It.IsAny<Action<EventInfo>>()), Times.Exactly(tenantIterations));
            EventHubClient.Verify(v => v.StartAsync(), Times.Once);

            TenantService.Verify(v => v.GetActiveTenants(), Times.Once);

            ConfigurationService.Verify(v => v.Get(), Times.AtLeastOnce());
            BackupService.Verify(v => v.ProcessEvent(It.IsAny<EventMapping>(), LoanService.Object, Logger.Object, ActivityLogService.Object), Times.Exactly(tenantIterations));
        }

        [Fact]
        public void ProcessEvent_WhenAllInfoOk()
        {
            // arrange
            var eventhubObject = new
            {
                Name = "UnitTest",
                LoanReferenceNumber = "loan001",
            };
            var logContext = NullLogContext.Instance;
            var logInfo = It.IsAny<string>();
            var eventName = It.IsAny<string>();

            //-------------------------------
            Logger.Setup(s => s.Info(logInfo));
            Logger.Setup(s => s.Error(logInfo));
            LoggerFactory.Setup(s => s.Create(logContext)).Returns(Logger.Object);
            //-------------------------------
            EventHubClient.Setup(s => s.StartAsync());
            EventHubClient.Setup(s => s.On(eventName, It.IsAny<Action<EventInfo>>()));
            EventHubClientFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(EventHubClient.Object);
            //-------------------------------
            TenantService.Setup(s => s.GetActiveTenants()).Returns(InMemoryTenants.ToList());
            TenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(TenantService.Object);
            //-------------------------------
            TokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
            //-------------------------------
            ConfigurationService.Setup(s => s.Get()).Returns(InMemoryConfiguration);
            ConfigurationServiceFactory.Setup(c => c.Create(It.IsAny<StaticTokenReader>())).Returns(ConfigurationService.Object);
            //-------------------------------
            LoanService.Setup(l => l.GetLoanDetails(It.IsAny<string>())).Returns(GetLoanDetails("loan001"));
            LoanServiceFactory.Setup(l => l.Create(It.IsAny<StaticTokenReader>())).Returns(LoanService.Object);
            //-------------------------------
            var activityLogEntries = new[] { InMemoryActivity, InMemoryActivity, InMemoryActivity }
            .Select((a, i) =>
            {
                i++;
                a.EntityType = "loan";
                a.EntityId = $"loan00{i}";
                return a;
            });
            ActivityLogService.Setup(x => x.GetActivities(It.IsAny<string>(), It.IsAny<string>(), 3, 3)).Returns(activityLogEntries);
            ActivityLogFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(ActivityLogService.Object);
            //-------------------------------
            BackupService.Setup(s => s.ProcessEvent(InMemoryConfiguration.Events[0], LoanService.Object, Logger.Object, ActivityLogService.Object));
            BackupServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>(), Logger.Object, It.IsAny<IConfigurationServiceFactory<BackupConfiguration>>())).Returns(BackupService.Object);
            //-------------------------------
            BackupFormulaFactory.Setup(x => x.Create(It.IsAny<ILoanDetails>(), It.IsAny<IConfigurationService<BackupConfiguration>>())).Returns(BackupFormula.Object);

            BackupRepository.Setup(s => s.AddOrUpdate(It.IsAny<IBackupEntry>()));

            // act
            var service = Service;
            var callback = service.ProcessEvent(InMemoryConfiguration.Events[0], LoanService.Object, Logger.Object, ActivityLogService.Object);
            callback.Invoke(new EventInfo
            {
                Time = new Foundation.Date.TimeBucket(DateTimeOffset.Now),
                Name = "EventName",
                TenantId = TenantId,
                Data = eventhubObject
            });

            // assert
            BackupRepository.Verify(v => v.AddOrUpdate(It.IsAny<IBackupEntry>()), Times.Once);
            Logger.Verify(v => v.Info(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void GetBackups_UsingStartAndPage()
        {
            // arrange
            //var eventhubObject = new
            //{
            //    Name = "UnitTest",
            //    LoanReferenceNumber = "loan001",
            //};
            var logContext = NullLogContext.Instance;
            var logInfo = It.IsAny<string>();
            var eventName = It.IsAny<string>();

            //-------------------------------
            Logger.Setup(s => s.Info(logInfo));
            Logger.Setup(s => s.Error(logInfo));
            LoggerFactory.Setup(s => s.Create(logContext)).Returns(Logger.Object);
            //-------------------------------
            EventHubClient.Setup(s => s.StartAsync());
            EventHubClient.Setup(s => s.On(eventName, It.IsAny<Action<EventInfo>>()));
            EventHubClientFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(EventHubClient.Object);
            //-------------------------------
            TenantService.Setup(s => s.GetActiveTenants()).Returns(InMemoryTenants.ToList());
            TenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(TenantService.Object);
            //-------------------------------
            TokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
            //-------------------------------
            ConfigurationService.Setup(s => s.Get()).Returns(InMemoryConfiguration);
            ConfigurationServiceFactory.Setup(c => c.Create(It.IsAny<StaticTokenReader>())).Returns(ConfigurationService.Object);
            //-------------------------------
            LoanService.Setup(l => l.GetLoanDetails(It.IsAny<string>())).Returns(GetLoanDetails("loan001"));
            LoanServiceFactory.Setup(l => l.Create(It.IsAny<StaticTokenReader>())).Returns(LoanService.Object);
            //-------------------------------
            var activityLogEntries = new[] { InMemoryActivity, InMemoryActivity, InMemoryActivity }
            .Select((a, i) =>
            {
                i++;
                a.EntityType = "loan";
                a.EntityId = $"loan00{i}";
                return a;
            });
            ActivityLogService.Setup(x => x.GetActivities(It.IsAny<string>(), It.IsAny<string>(), 3, 3)).Returns(activityLogEntries);
            ActivityLogFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(ActivityLogService.Object);
            //-------------------------------
            BackupService.Setup(s => s.ProcessEvent(InMemoryConfiguration.Events[0], LoanService.Object, Logger.Object, ActivityLogService.Object));
            BackupServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>(), Logger.Object, It.IsAny<IConfigurationServiceFactory<BackupConfiguration>>())).Returns(BackupService.Object);
            //-------------------------------

            BackupRepository.Setup(s => s.AddOrUpdate(It.IsAny<IBackupEntry>()));

            //var startDate = new DateTimeOffset(DateTime.Now.AddMinutes(-1));
            //Expression<Func<IBackupEntry, bool>> queryByDate = q => q.SnapshotTime.Time > startDate;


            // TODO: Problems with "Task" inside this test

            //BackupRepository.Setup(s => s.Since(queryByDate, It.IsAny<int>(), It.IsAny<int>())).Callback<IEnumerable<IBackupEntry>>(b =>
            //Returns(Task.FromResult(new[] { InMemoryBackupEntry }.AsEnumerable())).Callback<IEnumerable<IBackupEntry>>(b => 
            //{

            // assert
            //BackupRepository.Verify(v => v.Since(queryByDate, It.IsAny<int>(), It.IsAny<int>()), Times.Once);

            //});

            //var list = new[] { InMemoryBackupEntry, InMemoryBackupEntry }.AsEnumerable();            
            //BackupRepository.Setup(s => s.Since(queryByDate, null, null))
            //                .Returns(Task.FromResult(list));            

            //// act            
            ////var aux = task.Result;
            //var result = Service.GetBackups(startDate, 1);
            //BackupRepository.Verify(v => v.Since(queryByDate, It.IsAny<int>(), It.IsAny<int>()).Result, Times.Once);

        }

        [Fact]
        public void GetBackup_WhenExecutionOK()
        {
            // arrange
            var logContext = NullLogContext.Instance;
            var logInfo = It.IsAny<string>();
            var eventName = It.IsAny<string>();

            //-------------------------------
            Logger.Setup(s => s.Info(logInfo));
            Logger.Setup(s => s.Error(logInfo));
            LoggerFactory.Setup(s => s.Create(logContext)).Returns(Logger.Object);
            //-------------------------------
            EventHubClient.Setup(s => s.StartAsync());
            EventHubClient.Setup(s => s.On(eventName, It.IsAny<Action<EventInfo>>()));
            EventHubClientFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(EventHubClient.Object);
            //-------------------------------
            TenantService.Setup(s => s.GetActiveTenants()).Returns(InMemoryTenants.ToList());
            TenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(TenantService.Object);
            //-------------------------------
            TokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
            //-------------------------------
            ConfigurationService.Setup(s => s.Get()).Returns(InMemoryConfiguration);
            ConfigurationServiceFactory.Setup(c => c.Create(It.IsAny<StaticTokenReader>())).Returns(ConfigurationService.Object);
            //-------------------------------
            LoanService.Setup(l => l.GetLoanDetails(It.IsAny<string>())).Returns(GetLoanDetails("loan001"));
            LoanServiceFactory.Setup(l => l.Create(It.IsAny<StaticTokenReader>())).Returns(LoanService.Object);
            //-------------------------------
            var activityLogEntries = new[] { InMemoryActivity, InMemoryActivity, InMemoryActivity }
            .Select((a, i) =>
            {
                i++;
                a.EntityType = "loan";
                a.EntityId = $"loan00{i}";
                return a;
            });
            ActivityLogService.Setup(x => x.GetActivities(It.IsAny<string>(), It.IsAny<string>(), 3, 3)).Returns(activityLogEntries);
            ActivityLogFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(ActivityLogService.Object);
            //-------------------------------
            BackupService.Setup(s => s.ProcessEvent(InMemoryConfiguration.Events[0], LoanService.Object, Logger.Object, ActivityLogService.Object));
            BackupServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>(), Logger.Object, It.IsAny<IConfigurationServiceFactory<BackupConfiguration>>())).Returns(BackupService.Object);
            //-------------------------------
            BackupFormulaFactory.Setup(x => x.Create(It.IsAny<ILoanDetails>(), It.IsAny<IConfigurationService<BackupConfiguration>>())).Returns(BackupFormula.Object);


            BackupRepository.Setup(s => s.GetBackup(It.IsAny<string>())).Returns(Mock.Of<IBackupEntry>());

            // act            
            var result = Service.GetBackup("loan001");

            Assert.NotNull(result);

            // assert
            BackupRepository.Verify(s => s.GetBackup(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void GetBackup_WhenHasNoLoanReferenceNumber()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var logContext = NullLogContext.Instance;
                var logInfo = It.IsAny<string>();
                var eventName = It.IsAny<string>();

                //-------------------------------
                Logger.Setup(s => s.Info(logInfo));
                Logger.Setup(s => s.Error(logInfo));
                LoggerFactory.Setup(s => s.Create(logContext)).Returns(Logger.Object);
                //-------------------------------
                EventHubClient.Setup(s => s.StartAsync());
                EventHubClient.Setup(s => s.On(eventName, It.IsAny<Action<EventInfo>>()));
                EventHubClientFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(EventHubClient.Object);
                //-------------------------------
                TenantService.Setup(s => s.GetActiveTenants()).Returns(InMemoryTenants.ToList());
                TenantServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>())).Returns(TenantService.Object);
                //-------------------------------
                TokenHandler.Setup(s => s.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
                //-------------------------------
                ConfigurationService.Setup(s => s.Get()).Returns(InMemoryConfiguration);
                ConfigurationServiceFactory.Setup(c => c.Create(It.IsAny<StaticTokenReader>())).Returns(ConfigurationService.Object);
                //-------------------------------
                LoanService.Setup(l => l.GetLoanDetails(It.IsAny<string>())).Returns(GetLoanDetails("loan001"));
                LoanServiceFactory.Setup(l => l.Create(It.IsAny<StaticTokenReader>())).Returns(LoanService.Object);
                //-------------------------------
                var activityLogEntries = new[] { InMemoryActivity, InMemoryActivity, InMemoryActivity }
                .Select((a, i) =>
                {
                    i++;
                    a.EntityType = "loan";
                    a.EntityId = $"loan00{i}";
                    return a;
                });
                ActivityLogService.Setup(x => x.GetActivities(It.IsAny<string>(), It.IsAny<string>(), 3, 3)).Returns(activityLogEntries);
                ActivityLogFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(ActivityLogService.Object);
                //-------------------------------
                BackupService.Setup(s => s.ProcessEvent(InMemoryConfiguration.Events[0], LoanService.Object, Logger.Object, ActivityLogService.Object));
                BackupServiceFactory.Setup(s => s.Create(It.IsAny<StaticTokenReader>(), Logger.Object, It.IsAny<IConfigurationServiceFactory<BackupConfiguration>>())).Returns(BackupService.Object);
                //-------------------------------

                BackupRepository.Setup(s => s.GetBackup(It.IsAny<string>()));

                // act            
                var result = Service.GetBackup(null);

                Assert.Null(result);

                // assert
                BackupRepository.Verify(s => s.GetBackup(It.IsAny<string>()), Times.Once);
            });
        }
    }
}