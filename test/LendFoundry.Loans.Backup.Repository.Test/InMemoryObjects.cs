using LendFoundry.ActivityLog;
using LendFoundry.ActivityLog.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Client.Models;
using LendFoundry.Loans.Schedule;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Backup.Repository.Test
{
    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        // configuration
        protected Mock<IConfigurationServiceFactory<BackupConfiguration>> ConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory<BackupConfiguration>>();
        protected Mock<IConfigurationService<BackupConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<BackupConfiguration>>();

        // token handler
        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        // eventhub
        protected Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();
        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        // logger
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        // tenant 
        protected Mock<ITenantServiceFactory> TenantServiceFactory { get; } = new Mock<ITenantServiceFactory>();
        protected Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();

        // repository
        protected Mock<IBackupRepository> BackupRepository { get; } = new Mock<IBackupRepository>();

        // service
        protected Mock<IBackupServiceFactory> BackupServiceFactory { get; } = new Mock<IBackupServiceFactory>();
        protected Mock<IBackupServiceExtended> BackupService { get; } = new Mock<IBackupServiceExtended>();

        protected Mock<ILoanServiceFactory> LoanServiceFactory { get; } = new Mock<ILoanServiceFactory>();
        protected Mock<ILoanService> LoanService { get; } = new Mock<ILoanService>();

        protected Mock<IBackupFormulaFactory> BackupFormulaFactory { get; } = new Mock<IBackupFormulaFactory>();
        protected Mock<IBackupFormula> BackupFormula { get; } = new Mock<IBackupFormula>();

        // activity-log
        protected Mock<IActivityLogClientFactory> ActivityLogFactory { get; } = new Mock<IActivityLogClientFactory>();
        protected Mock<IActivityLogService> ActivityLogService { get; } = new Mock<IActivityLogService>();
        protected IActivityLog InMemoryActivity => new ActivityLog.ActivityLog
        {
            EntityId = "EntityId",
            EntityType = "EntityType",
            Title = "Title",
            Description = "Description",
            Author = "Author",
            Event = new EventInfo
            {
                Data = null,
                Id = "EventId",
                Name = "EventName",
                Source = "Source",
                TenantId = TenantId,
                Time = new TimeBucket(new DateTimeOffset(DateTime.Now))
            }
        };

        public IBackupEntry InMemoryBackupEntry
        {
            get
            {
                var backup = new BackupEntry
                {
                    SnapshotTime = new TimeBucket(),
                    Loan = GetLoan(),
                    PaymentSchedule = GetPaymentSchedule(),
                    Summary = GetSummary(),
                    Transactions = GetTransactions()
                };

                return backup;
            }
        }

        public static ILoanDetails GetLoanDetails(string loanReferenceNumber)
        {
            var loanInfo = GetLoanInfo(loanReferenceNumber);
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);

            var loanDetails = new LoanDetails
            {
                LoanInfo = loanAdapter,
                Transactions = GetTransactions(),
                PaymentSchedule = GetPaymentSchedule(loanReferenceNumber)
            };

            return loanDetails;
        }
        
        public static ILoan GetLoan(string referenceNumber = null)
        {
            return
                new Loan
                {
                    BankAccounts = new List<IBankAccount>
                    {
                        new Client.Models.BankAccount
                        {
                            AccountNumber = "219",
                            AccountType = BankAccountType.Checking,
                            RoutingNumber = "001"
                        }
                    },
                    Borrowers = new[]
                    {
                        new Borrower
                        {
                            ReferenceNumber = "0001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "F",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                            SSN = "103-58-4851",
                            Email = "johndoe@lendfoundry.com",
                            Addresses = new[]
                            {
                                new Address
                                {
                                    IsPrimary = true,
                                    Line1 = "145 5th Ave",
                                    City = "Irvine",
                                    State = "CA",
                                    ZipCode = "12952"
                                }
                            },
                            Phones = new[]
                            {
                                new Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "23938439"
                                }
                            }
                        }
                    },
                    CampaignCode = "190C-2",
                    FundingSource = "crb",
                    Grade = "B",
                    HomeOwnership = "Owner",
                    Investor = new LoanInvestor
                    {
                        Id = "1001",
                        LoanPurchaseDate = new DateTimeOffset(DateTime.Now)
                    },
                    MonthlyIncome = 7000,
                    PaymentMethod = PaymentMethod.ACH,
                    PostCloseDti = 6.4,
                    PreCloseDti = 5.7,
                    Purpose = "Home improvement",
                    ReferenceNumber = referenceNumber,
                    Scores = new[]
                    {
                        new Score
                        {
                            Name = "ficoRiskScore09",
                            InitialDate = new DateTimeOffset(DateTime.Now),
                            InitialScore = "500",
                            UpdatedDate = new DateTimeOffset(DateTime.Now),
                            UpdatedScore = "601"
                        }
                    },
                    Terms = new LoanTerms
                    {
                        ApplicationDate = new DateTimeOffset(DateTime.Now),
                        APR = 100,
                        FundedDate = new DateTimeOffset(DateTime.Now),
                        LoanAmount = 1000,
                        OriginationDate = new DateTimeOffset(DateTime.Now),
                        Term = 3,
                        Rate = 8.24,
                        RateType = RateType.Percent,
                        Fees = new[]
                        {
                            new Fee
                            {
                                Amount = 212,
                                Code = "142",
                                Type = FeeType.Fixed
                            }
                        }
                    }
                };
        }

        public static LoanInfo GetLoanInfo(string loanReferenceNumber = null)
        {
            var loanInfo = new LoanInfo
            {
                ReferenceNumber = loanReferenceNumber,
                FundingSource = "crb",
                Purpose = "Home improvement",
                Status = new Client.Models.LoanStatus(),
                Investor = new LoanInvestor
                {
                    Id = "1001",
                    LoanPurchaseDate = new DateTimeOffset(DateTime.Now)
                },
                CurrentDue = 2,
                DaysPastDue = 2,
                LastPaymentAmount = 1110,
                LastPaymentDate = new DateTimeOffset(DateTime.Now),
                NextDueDate = new DateTimeOffset(DateTime.Now),
                NextInstallmentAmount = 10,
                RemainingBalance = 10,
                Scores = new[]
                {
                    new Score
                    {
                        Name = "ficoRiskScore09",
                        InitialDate = new DateTimeOffset(DateTime.Now),
                        InitialScore = "500",
                        UpdatedDate = new DateTimeOffset(DateTime.Now),
                        UpdatedScore = "601"
                    }
                },
                Grade = "B",
                PreCloseDti = 5.7,
                PostCloseDti = 6.4,
                MonthlyIncome = 7000,
                HomeOwnership = "Owner",
                CampaignCode = "190C-2",
                Borrowers = new[]
                {
                    new Client.Models.Borrower
                    {
                        ReferenceNumber = "0001",
                        IsPrimary = true,
                        FirstName = "John",
                        MiddleInitial = "F",
                        LastName = "Doe",
                        DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                        SSN = "103-58-4851",
                        Email = "johndoe@lendfoundry.com",
                        Addresses = new[]
                        {
                            new Client.Models.Address
                            {
                                IsPrimary = true,
                                Line1 = "145 5th Ave",
                                City = "Irvine",
                                State = "CA",
                                ZipCode = "12952"
                            }
                        },
                        Phones = new[]
                        {
                            new Client.Models.Phone
                            {
                                Type = PhoneType.Home,
                                IsPrimary = true,
                                Number = "23938439"
                            }
                        }
                    }
                },
                Terms = new Client.Models.LoanTerms
                {
                    ApplicationDate = new DateTimeOffset(DateTime.Now),
                    APR = 100,
                    FundedDate = new DateTimeOffset(DateTime.Now),
                    LoanAmount = 1000,
                    OriginationDate = new DateTimeOffset(DateTime.Now),
                    Term = 3,
                    Rate = 8.24,
                    RateType = RateType.Percent,
                    Fees = new[]
                    {
                        new Client.Models.Fee
                        {
                            Amount = 212,
                            Code = "142",
                            Type = FeeType.Fixed
                        }
                    }
                },
                BankAccounts = new List<Client.Models.BankAccount>
                {
                    new Client.Models.BankAccount
                    {
                        AccountNumber = "219",
                        AccountType = BankAccountType.Checking,
                        RoutingNumber = "001"
                    }
                }.ToArray(),
                PaymentMethod = PaymentMethod.ACH
            };
            return loanInfo;
        }

        public static IPaymentSchedule GetPaymentSchedule(string loanReferenceNumber = null)
        {
            return
                new PaymentSchedule
                {
                    LoanReferenceNumber = loanReferenceNumber,
                    TenantId = string.Empty,
                    Installments = new[]
                    {
                        new Installment {
                            PaymentAmount = 100,
                            DueDate = new DateTimeOffset(DateTime.Now)
                        }
                    }
                };
        }

        public static IPaymentScheduleSummary GetSummary()
        {
            return
                new PaymentScheduleSummary
                {
                    CurrentDue = 2,
                    DaysPastDue = 2,
                    LastPaymentAmount = 1000,
                    LastPaymentDate = new DateTimeOffset(DateTime.Now),
                    NextDueDate = new DateTimeOffset(DateTime.Now),
                    NextInstallmentAmount = 10,
                    RemainingBalance = 1000
                };
        }

        public static List<ITransaction> GetTransactions()
        {
            return new List<ITransaction>
            {
                new Transaction {
                    Amount = 10,
                    Date = new DateTimeOffset(DateTime.Now)
                }
            };
        }



        public class InMemoryLoanBackupAdapter : ILoanInfo
        {
            public InMemoryLoanBackupAdapter(LoanInfo model)
            {
                Loan = new Loan
                {
                    BankAccounts = new List<Client.Models.BankAccount>
                    {
                        new Client.Models.BankAccount
                        {
                            AccountNumber = "219",
                            AccountType = BankAccountType.Checking,
                            RoutingNumber = "001"
                        }
                    },
                    Borrowers = new[]
                    {
                        new Borrower
                        {
                            ReferenceNumber = "0001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "F",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                            SSN = "103-58-4851",
                            Email = "johndoe@lendfoundry.com",
                            Addresses = new[]
                            {
                                new Address
                                {
                                    IsPrimary = true,
                                    Line1 = "145 5th Ave",
                                    City = "Irvine",
                                    State = "CA",
                                    ZipCode = "12952"
                                }
                            },
                            Phones = new[]
                            {
                                new Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "23938439"
                                }
                            }
                        }
                    },
                    CampaignCode = "190C-2",
                    FundingSource = "crb",
                    Grade = "B",
                    HomeOwnership = "Owner",
                    Investor = new LoanInvestor
                    {
                        Id = "1001",
                        LoanPurchaseDate = new DateTimeOffset(DateTime.Now)
                    },
                    MonthlyIncome = 7000,
                    PaymentMethod = PaymentMethod.ACH,
                    PostCloseDti = 6.4,
                    PreCloseDti = 5.7,
                    Purpose = "Home improvement",
                    ReferenceNumber = model.ReferenceNumber,
                    Scores = new[]
                    {
                        new Score
                        {
                            Name = "ficoRiskScore09",
                            InitialDate = new DateTimeOffset(DateTime.Now),
                            InitialScore = "500",
                            UpdatedDate = new DateTimeOffset(DateTime.Now),
                            UpdatedScore = "601"
                        }
                    },
                    Terms = new LoanTerms
                    {
                        ApplicationDate = new DateTimeOffset(DateTime.Now),
                        APR = 100,
                        FundedDate = new DateTimeOffset(DateTime.Now),
                        LoanAmount = 1000,
                        OriginationDate = new DateTimeOffset(DateTime.Now),
                        Term = 3,
                        Rate = 8.24,
                        RateType = RateType.Percent,
                        Fees = new[]
                        {
                            new Fee
                            {
                                Amount = 212,
                                Code = "142",
                                Type = FeeType.Fixed
                            }
                        }
                    }
                };

                Summary = new PaymentScheduleSummary
                {
                    CurrentDue = model.CurrentDue,
                    DaysPastDue = model.DaysPastDue,
                    LastPaymentAmount = model.LastPaymentAmount,
                    LastPaymentDate = model.LastPaymentDate,
                    NextDueDate = model.NextDueDate,
                    NextInstallmentAmount = model.NextInstallmentAmount,
                    RemainingBalance = model.RemainingBalance
                };
            }

            public ILoan Loan { get; }

            public IPaymentScheduleSummary Summary { get; }
        }
    }
}