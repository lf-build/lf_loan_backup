﻿using System;
using System.Linq;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Backup.Abstractions.Configuration;
using LendFoundry.Loans.Backup.Persistence;
using LendFoundry.Tenant.Client;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Backup.Repository.Test
{
    public class BackupRepositoryTests : InMemoryObjects
    {
        private Mock<ITenantService> TenantService { get; set; }

        private IBackupRepository Repository(IMongoConfiguration config)
        {
            TenantService = new Mock<ITenantService>();
            TenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = TenantId });
            return new BackupRepository
            (
                configuration: config,
                tenantService: TenantService.Object
            );
        }

        [MongoFact]
        public void AddOrUpdate_One()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var backup = InMemoryBackupEntry;
                backup.TenantId = TenantId;
                backup.Loan.ReferenceNumber = "loan001";

                // act
                repo.AddOrUpdate(backup);

                // assert
                var backupAdded = repo.Get(backup.Id).Result;
                Assert.NotNull(backupAdded);
                Assert.Equal(backupAdded.TenantId, TenantId);
                Assert.False(string.IsNullOrWhiteSpace(backupAdded.Id));
                Assert.Equal(backupAdded.Loan.ReferenceNumber, "loan001");
            });
        }

        [MongoFact]
        public void AddOrUpdate_Many()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var backups = new[] { InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry };
                backups = backups.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                backups.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var backupsAdded = repo.All(x => x.TenantId == TenantId).Result;
                Assert.NotNull(backupsAdded);
                Assert.NotEmpty(backupsAdded);
                Assert.False(backupsAdded.Any(x => string.IsNullOrWhiteSpace(x.Id)));
            });
        }

        [MongoFact]
        public void GetBackups()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var loanNumber = "loan001";
                var repo = Repository(config);
                var list = new[] { InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var backups = repo.All(x => x.Loan.ReferenceNumber == loanNumber).Result;
                Assert.NotNull(backups);
                Assert.NotEmpty(backups);
                Assert.True(backups.All(x => x.Loan.ReferenceNumber == loanNumber));
            });
        }

        [MongoFact]
        public void GetBackups_WhenUsingSkipTake()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                    InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                    InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration { PageSize = 100 });

                var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddMinutes(-2));
                var bkps = srv.GetBackups(snapshotTime);
                var backups = repo.All(x => x.SnapshotTime.Time > snapshotTime, 3, 3).Result;

                Assert.NotNull(backups);
                Assert.NotEmpty(backups);
                Assert.True(backups.All(x => x.SnapshotTime.Time > snapshotTime));
                Assert.Equal(3, backups.Count());
            });
        }

        [MongoFact]
        public void GetBackups_WhenUsingSkipTakeOutOfRange()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                    InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                    InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddMinutes(-2));
                var backups = repo.All(x => x.SnapshotTime.Time > snapshotTime, 6, 6).Result;
                Assert.Empty(backups);
                Assert.True(backups.All(x => x.SnapshotTime.Time > snapshotTime));
                Assert.Equal(0, backups.Count());
            });
        }

        [MongoFact]
        public void GetBackups_ByService_WhenUsingPage()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                    InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                    InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration { PageSize = 100 });
                var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddMinutes(-2));
                var backups = srv.GetBackups(snapshotTime, 1);

                Assert.NotNull(backups);
                Assert.NotEmpty(backups);
                Assert.True(backups.All(x => x.SnapshotTime.Time > snapshotTime));
                Assert.Equal(6, backups.Count());
            });
        }

        [MongoFact]
        public void GetBackups_ByService_WhithoutPageSize()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                MongoTest.Run(config =>
                {
                    // arrange
                    var repo = Repository(config);
                    var list = new[] {
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                    };
                    list = list.Select((s, i) =>
                    {
                        s.Loan.ReferenceNumber = $"loan00{i}";
                        s.TenantId = TenantId;
                        s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                        s.PaymentSchedule.TenantId = TenantId;

                        return s;
                    }).ToArray();

                    // act
                    list.ToList().ForEach(item => { repo.Add(item); });

                    // assert
                    var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                    configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration());

                    var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                    var snapshotTime = new DateTimeOffset(DateTime.Now.AddMinutes(-2));
                    var backups = srv.GetBackups(snapshotTime, 8);

                    Assert.NotNull(backups);
                    Assert.NotEmpty(backups);
                    Assert.True(backups.All(x => x.SnapshotTime.Time > snapshotTime));
                    Assert.Equal(3, backups.Count());
                });
            });
        }

        [MongoFact]
        public void GetBackups_ByService_WithPageZero()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                    };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();



                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration { PageSize = 100 });

                var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddDays(-2));
                var backups = srv.GetBackups(snapshotTime, 0);

                Assert.NotNull(backups);
                Assert.Empty(backups);
            });
        }

        [MongoFact]
        public void GetBackups_ByService_WithPageOne()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                    };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration { PageSize = 20 });

                var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddDays(-2));
                var backups = srv.GetBackups(snapshotTime, 1);

                Assert.NotNull(backups);
                Assert.NotEmpty(backups);
                Assert.Equal(20, backups.Count());
            });
        }

        [MongoFact]
        public void GetBackups_ByService_WithPageMinusOne()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                    };
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration { PageSize = 100 });

                var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddDays(-2));
                var backups = srv.GetBackups(snapshotTime, -1);

                Assert.NotNull(backups);
                Assert.Empty(backups);
            });
        }

        [MongoFact]
        public void GetBackups_ByService_ManyPages()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);
                var list = new[] {
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry,
                        InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry
                };
                // 52 items
                list = list.Select((s, i) =>
                {
                    s.Loan.ReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    s.PaymentSchedule.TenantId = TenantId;

                    return s;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var configurationService = new Mock<IConfigurationService<BackupConfiguration>>();
                configurationService.Setup(c => c.Get()).Returns(new BackupConfiguration { PageSize = 20 });

                var srv = new BackupService(repo, Mock.Of<ILogger>(), configurationService.Object, Mock.Of<IBackupFormulaFactory>());
                var snapshotTime = new DateTimeOffset(DateTime.Now.AddDays(-2));
                var backups = srv.GetBackups(snapshotTime, 1);

                // First Page
                Assert.NotNull(backups);
                Assert.NotEmpty(backups);
                Assert.Equal(20, backups.Count());


                var page2 = srv.GetBackups(snapshotTime, 2);
                // Second Page
                Assert.NotNull(page2);
                Assert.NotEmpty(page2);
                Assert.False(page2.Any(x => x.Loan.ReferenceNumber == "loan0018"));
                Assert.Equal(20, page2.Count());

                var page3 = srv.GetBackups(snapshotTime, 3);
                // Second Page
                Assert.NotNull(page3);
                Assert.NotEmpty(page3);
                Assert.True(page3.Any(x => x.Loan.ReferenceNumber == "loan0051"));
                Assert.Equal(12, page3.Count());

            });
        }


        [MongoFact]
        public void GetBackup()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var loanNumber = "loan001";
                var repo = Repository(config);
                var list = new[] { InMemoryBackupEntry, InMemoryBackupEntry, InMemoryBackupEntry };
                list = list.Select((b, i) =>
                {
                    b.Loan.ReferenceNumber = $"loan00{i}";
                    b.TenantId = TenantId;
                    b.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                    b.PaymentSchedule.TenantId = TenantId;

                    return b;
                }).ToArray();

                // act
                list.ToList().ForEach(item => { repo.AddOrUpdate(item); });

                // assert
                var backup = repo.GetBackup(loanNumber);
                Assert.NotNull(backup);
                Assert.Equal(loanNumber, backup.Loan.ReferenceNumber);
            });
        }

        [MongoFact]
        public void GetBackup_WhenHasNoLoanReferenceNumber()
        {
            MongoTest.Run(config =>
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    // arrange
                    var repo = Repository(config);
                    
                    // assert
                    var backup = repo.GetBackup(null);
                });
            });
        }
    }
}