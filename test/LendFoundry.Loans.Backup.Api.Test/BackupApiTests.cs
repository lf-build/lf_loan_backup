﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Backup.Abstractions;
using LendFoundry.Loans.Backup.Api.Controllers;
using LendFoundry.Loans.Backup.Repository.Test;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Sdk;

namespace LendFoundry.Loans.Backup.Api.Test
{
    public class BackupApiTests : InMemoryObjects
    {
        private Mock<IBackupService> Service { get; } = new Mock<IBackupService>();

        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        [Fact]
        public void GetBackups_Using_StartDate_And_Page_WhenExecutionOk()
        {
            // arrange
            var list = new[] { InMemoryBackupEntry, InMemoryBackupEntry }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                s.Loan.ReferenceNumber = $"loan00{i}";
                s.TenantId = TenantId;
                s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                s.PaymentSchedule.TenantId = TenantId;

                return s;
            }).ToArray();

            Service.Setup(x => x.GetBackups(It.IsAny<DateTimeOffset>(), It.IsAny<int>())).Returns(list);
            
            // act
            var ctrl = new ApiController(Service.Object, Logger.Object);
            var startDate = new DateTimeOffset(DateTime.Now);
            var page = 2;
            var actionResult = ctrl.GetBackups(startDate, page);

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)actionResult).StatusCode);
            Service.Verify(x => x.GetBackups(It.IsAny<DateTimeOffset>(), It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void GetBackups_Without_StartDate_And_Page_WhenExecutionOk()
        {
            // arrange
            var list = new[] { InMemoryBackupEntry, InMemoryBackupEntry }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                s.Loan.ReferenceNumber = $"loan00{i}";
                s.TenantId = TenantId;
                s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                s.PaymentSchedule.TenantId = TenantId;

                return s;
            }).ToArray();

            Service.Setup(x => x.GetBackups(null, null)).Returns(list);

            // act
            var ctrl = new ApiController(Service.Object, Logger.Object);
            var actionResult = ctrl.GetBackups(null, null);

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)actionResult).StatusCode);
            Service.Verify(x => x.GetBackups(null, null), Times.Once);
        }

        [Fact]
        public void GetBackups_Without_StartDate_And_Page_NotFoundRecords()
        {
            // arrange
            var list = new List<IBackupEntry>();

            Service.Setup(x => x.GetBackups(null, null)).Returns(list);

            // act
            var ctrl = new ApiController(Service.Object, Logger.Object);
            var actionResult = ctrl.GetBackups(null, null);

            // assert
            Assert.Equal(404, ((ErrorResult)actionResult).StatusCode);
            Service.Verify(x => x.GetBackups(null, null), Times.Once);
        }

        [Fact]
        public void GetBackups_Without_StartDate_And_Page_ThrowException()
        {
            // arrange
            var list = new List<IBackupEntry>();

            Service.Setup(x => x.GetBackups(null, null));

            // act
            var ctrl = new ApiController(Service.Object, Logger.Object);
            var actionResult = ctrl.GetBackups(null, null);

            // assert
            Assert.Equal(400, ((ErrorResult)actionResult).StatusCode);
            Service.Verify(x => x.GetBackups(null, null), Times.Once);
        }

        [Fact]
        public void GetBackups_Using_StartDate_WhenExecutionOk()
        {
            // arrange
            var list = new[] {
                InMemoryBackupEntry, InMemoryBackupEntry,
                InMemoryBackupEntry, InMemoryBackupEntry,
                InMemoryBackupEntry, InMemoryBackupEntry
            }.AsEnumerable();

            var snapshotTime = new DateTimeOffset(DateTime.Now);

            list = list.Select((s, i) =>
            {
                s.Loan.ReferenceNumber = $"loan00{i}";
                s.SnapshotTime = new Foundation.Date.TimeBucket(snapshotTime.AddMinutes(2));
                s.TenantId = TenantId;
                s.PaymentSchedule.LoanReferenceNumber = $"loan00{i}";
                s.PaymentSchedule.TenantId = TenantId;

                return s;
            }).ToArray();

            Service.Setup(x => x.GetBackups(It.IsAny<DateTimeOffset>(), null)).Returns(list);

            // act
            var ctrl = new ApiController(Service.Object, Logger.Object);
            var startDate = new DateTimeOffset(DateTime.Now);
            var actionResult = ctrl.GetBackups(startDate, null);

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)actionResult).StatusCode);
            var bkps = ((HttpOkObjectResult)actionResult).Value as IEnumerable<IBackupEntry>;
            Assert.NotEmpty(bkps);
            Service.Verify(x => x.GetBackups(It.IsAny<DateTimeOffset>(), null), Times.Once);
        }
        
        [Fact]
        public void GetBackup_WhenExecutionOk()
        {
            // arrange
            var loanNumber = "loan001";

            Service.Setup(x => x.GetBackup(It.IsAny<string>())).Returns(Mock.Of<BackupEntry>());

            // act
            using (var ctrl = new ApiController(Service.Object, Logger.Object))
            {
                var actionResult = ctrl.GetBackup(loanNumber);

                // assert
                Assert.Equal(200, ((HttpOkObjectResult)actionResult).StatusCode);
            }
            
            Service.Verify(x => x.GetBackup(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void GetBackup_WhenExecutionFailed()
        {
            // arrange
            var loanNumber = "loan001";

            Service.Setup(x => x.GetBackup(It.IsAny<string>()));

            // act
            using (var ctrl = new ApiController(Service.Object, Logger.Object))
            {
                var actionResult = ctrl.GetBackup(loanNumber);

                // assert
                Assert.Equal(404, ((ObjectResult)actionResult).StatusCode);
            }

            Service.Verify(x => x.GetBackup(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void GetBackup_When_HasNoLoanReferenceNumber()
        {
            // arrange
            Service.Setup(x => x.GetBackup(null));

            // act
            using (var ctrl = new ApiController(Service.Object, Logger.Object))
            {
                var actionResult = ctrl.GetBackup(null);

                // assert
                Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
            }

            Service.Verify(x => x.GetBackup(It.IsAny<string>()), Times.Never);
        }
    }
}